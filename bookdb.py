from os import name
import sqlite3
from sqlite3.dbapi2 import SQLITE_SELECT
import time
# Connect to DB
con = sqlite3.connect('bookdb.db')
print("Connected to bookdb.db")
cursor = con.cursor()

# Main menu 
def mainmenu():
    time.sleep(1)
    print("BookDB menu:")
    time.sleep(2)
    menu = input ("1. Create-DB 2. Show-DB 3. Search-Engine 4. ADD-Book-to-DB 5. EXIT ")
    if menu == "1":
        CRTDB()
    elif menu == "2":
        SHOWDB()
    elif menu == "3":
        searchenginedb()
    elif menu =="4":
        insertdata()
    elif menu =="5":
        raise SystemExit
    else:
        print("WRONG INPUT, TRY SOMETHING ELSE!")
        mainmenu()

# Put all information to DB. 
def putdate(Author, Year, Name, Genre, List,):
    put = '''INSERT INTO bookdb(Author, Year, Name, Genre, List)VALUES (?, ?, ?, ?, ?)'''
    cursor.execute(put,[(Author), (Year), (Name), (Genre), (List)])


# 1. Create  DB
def CRTDB():
    cursor.execute('CREATE TABLE IF NOT EXISTS bookdb(Author TEXT, Year NUMERIC, Name TEXT, Genre TEXT, List NUMERIC)')
     # Stephen King
    putdate("Stephen King", 1974,'Carrie','Roman',199,)
    putdate("Stephen King", 1977,'The Shining','Roman',447,)
    putdate("Stephen King", 1978,'Night Shift','Roman',823,)
    putdate("Stephen King", 1979,'Long March','Roman',384,)
    # J. R. R. Tolkien
    putdate("J. R. R. Tolkien", 1937,'The Hobbit','Fantasy',310,)
    putdate("J. R. R. Tolkien", 2016 ,'The Lay of Aotrou and Itroun','Poem',128,)
    putdate("J. R. R. Tolkien", 1967,'Smith of Wootton Major','Fantasy',310,)
    putdate("J. R. R. Tolkien", 1945,'Leaf by Niggle','Fantasy',30,)
    # Jo Nesbø 
    putdate("Jo Nesbø", 2005,'The Bat','CrimeNovel',380,)
    putdate("Jo Nesbø", 2013,'Cockroaches','CrimeNovel',240,)
    putdate("Jo Nesbø", 2008,'Nemesis','CrimeNovel',294,)
    putdate("Jo Nesbø", 2014,'The Police','CrimeNovel',402,)
    # Agatha Christie
    putdate("Agatha Christie", 1923,'The Murder on the Links','CrimeNovel',298,)
    putdate("Agatha Christie", 1927,'The Big Four','CrimeNovel',282,)
    putdate("Agatha Christie", 1936,'The A.B.C. Murders','CrimeNovel',256,)
    putdate("Agatha Christie", 1934,'Murder on the Orient Express','CrimeNovel',256,)
    # Tom Clancy 
    putdate("Tom Clancy", 1984,"The Hunt for Red October",'Thriller',387,)
    putdate("Tom Clancy", 1998,"Rainbow Six",'Thriller',740,) 
    putdate("Tom Clancy", 2013,"Command Authority","Thriller", 736,)
    putdate("Tom Clancy", 1993,"Without Remorse","Thriller",639,)
    con.commit()
    time.sleep(2)
    print("Database Created!")
    mainmenu() 


# 2. Show DB
def SHOWDB():
    cursor.execute('SELECT * FROM bookdb')
    sdb = cursor.fetchall()
    print(sdb)   
    mainmenu()

# 3. CHOOSE BY CRITERIA
def searchenginedb():
    criteria = input ("1. Authors-By-name 2. Search-by-book 3. Search-by-Genre 4. Back-To-Menu >> ")
    if criteria == "1":
        Authors()
    elif criteria =="2":
        bookname()
    elif criteria =="3":
        genrename()
    elif criteria =="4":
        mainmenu()

# authors function 
def Authors():
    authors = input("1. Stephen King 2. J. R. R. Tolkien 3. Jo Nesbø 4. Agatha Christie 5. Tom Clancy 6. BACK")
    if authors == "1":
       cursor.execute("SELECT * from bookdb where Author=:a",{"a": "Stephen King"})
       sk = cursor.fetchall()
       print(sk)
    elif authors =="2":
        cursor.execute("SELECT * from bookdb where Author=:b",{"b": "J. R. R. Tolkien"})
        jr = cursor.fetchall()
        print(jr)
    elif authors =="3":
        cursor.execute("SELECT * from bookdb where Author=:c",{"c": "Jo Nesbø"})
        jn = cursor.fetchall()
        print(jn)
    elif authors =="4":
        cursor.execute("SELECT * from bookdb where Author=:d",{"d": "Agatha Christie"})
        ach = cursor.fetchall()
        print(ach)
    elif authors =="5":
        cursor.execute("SELECT * from bookdb where Author=:e",{"e": "Tom Clancy"})
        tc = cursor.fetchall()
        print(tc)
    elif authors =="6":
        authors()
    
    else:
        print("WRONG INPUT, TRY SOMETHING ELSE!")
        authors()

# search engine-bookname 
def bookname():
    bookname = input("Input book that are you searching:  ")
    cursor.execute("SELECT * from bookdb where Name=:a",{"a": (bookname)})
    bn = cursor.fetchall()
    print(bn)
    mainmenu()

def genrename():
    print("Choose a Genre!")
    genrename = input("1. Roman 2. Fantasy 3. Poem 4. Crime Novel 5. Thriller ")
    if genrename == "1":
       cursor.execute("SELECT * from bookdb where Genre=:a",{"a": "Roman"})
       ro = cursor.fetchall()
       print(ro)
       mainmenu()
    elif genrename == "2":
        cursor.execute("SELECT * from bookdb where Genre=:b",{"b":"Fantasy"})
        fa = cursor.fetchall()
        print(fa)
        mainmenu()
    elif genrename =="3":
        cursor.execute("SELECT * from bookdb where Genre=:c",{"c":"Poem"})
        po = cursor.fetchall()
        print(po)
        mainmenu()
    elif genrename =="4":
        cursor.execute("SELECT * from bookdb where Genre=:d",{"b":"Crime Novel"})
        cn = cursor.fetchall()
        print(cn)
        mainmenu()
    elif genrename =="5":
        cursor.execute("SELECT * from bookdb where Genre=:e",{"e":"Thriller"})
        th = cursor.fetchall()
        print(th)
        mainmenu()
    
    else:
        print("WRONG INPUT, TRY SOMETHING ELSE!")
        genrename()

def insertdata():
    insertauthor = input("Insert author:")
    print("DATA INSERTED!")
    insertyear = input("Insert year:")
    print("DATA INSERTED!")
    insertname = input("Insert name:")
    print("DATA INSERTED!")
    insertgenre = input("Inser genre:")
    print("DATA INSERTED!")
    insertlist = input("Insert list:")
    print("DATA INSERTED!")
    putdate(insertauthor, insertyear, insertname, insertgenre, insertlist)
    mainmenu()

mainmenu()