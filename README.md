# DB library of books

A book database is a database that allows you to select a specific book when you enter certain criteria. 
# LIB:

- sqlite3

# Criteria

- Year
- Author
- Genre
- List

# Authors of books

- Stephen King
- J. R. R. Tolkien
- Jo Nesbø
- Agatha Christie
- Tom Clancy

# TO-DO 

- Make a DB [DONE]
- Make a console menu [DONE]
- Show function for data in DB [DONE]
- Make a search engine [DONE]
- Make a recovery file [DONE]
- Make a function that can easy put data to DB [DONE]

