from os import name
import sqlite3
import time
# Connect to DB
con = sqlite3.connect('bookdb.db')
#Cursor 
cursor = con.cursor()
def mainmenu():
    print("Welcome to BOOKDB")
    time.sleep(2)
    menu = input ("1. CREATE DB 2.PRINT ALL BOOKS 3. CHOOSE BY CRITERIA 4. EXIT")
    if menu == "1":
        CRTDB()
    elif menu == "2":
        cursor.execute('SELECT * FROM bookdb')
        all = cursor.fetchall()
        print(all)   

# Create  DB
def CRTDB():
    cursor.execute('CREATE TABLE IF NOT EXISTS bookdb(Author TEXT, Year NUMERIC, Name TEXT, Genre TEXT, List NUMERIC)')
    # Stephen King
    putdate("Stephen King", 1974,'Carrie','Roman',199,)
    putdate("Stephen King", 1977,'The Shining','Roman',447,)
    putdate("Stephen King", 1978,'Night Shift','Roman',823,)
    putdate("Stephen King", 1979,'Long March','Roman',384,)
    # J. R. R. Tolkien
    putdate("J. R. R. Tolkien", 1937,'The Hobbit','Fantasy',310,)
    putdate("J. R. R. Tolkien", 2016 ,'The Lay of Aotrou and Itroun','Poem',128,)
    putdate("J. R. R. Tolkien", 1967,'Smith of Wootton Major','Fantasy',310,)
    putdate("J. R. R. Tolkien", 1945,'Leaf by Niggle','Fantasy',30,)
    # Jo Nesbø 
    putdate("Jo Nesbø", 2005,'The Bat','CrimeNovel',380,)
    putdate("Jo Nesbø", 2013,'Cockroaches','CrimeNovel',240,)
    putdate("Jo Nesbø", 2008,'Nemesis','CrimeNovel',294,)
    putdate("Jo Nesbø", 2014,'The Police','CrimeNovel',402,)
    # Agatha Christie
    putdate("Agatha Christie", 1923,'The Murder on the Links','CrimeNovel',298,)
    putdate("Agatha Christie", 1927,'The Big Four','CrimeNovel',282,)
    putdate("Agatha Christie", 1936,'The A.B.C. Murders','CrimeNovel',256,)
    putdate("Agatha Christie", 1934,'Murder on the Orient Express','CrimeNovel',256,)
    # Tom Clancy 
    putdate("Tom Clancy", 1984,"The Hunt for Red October",'Thriller',387,)
    putdate("Tom Clancy", 1998,"Rainbow Six",'Thriller',740,) 
    putdate("Tom Clancy", 2013,"Command Authority","Thriller", 736,)
    putdate("Tom Clancy", 1993,"Without Remorse","Thriller",639,)
# Add Author
def putdate(Author, Year, Name, Genre, List,):
    put = '''INSERT INTO bookdb(Author, Year, Name, Genre, List)VALUES (?, ?, ?, ?, ?)'''
    cursor.execute(put,[(Author), (Year), (Name), (Genre), (List)])
    con.commit() 


mainmenu()